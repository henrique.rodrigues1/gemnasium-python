module gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/v2

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.23.0
	gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2 v2.26.1
)

go 1.13

replace gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2 => ./gemnasium
